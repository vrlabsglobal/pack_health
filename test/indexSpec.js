/* eslint-disable no-unused-expressions */

const chai = require('chai');
const request = require('request-promise-native');
const sinon = require('sinon');
const index = require('../index');
var blueBird = require('bluebird');
const { expect } = chai;

describe('coding challenge spec', () => {
  beforeEach(function() {
    sinon.stub(request,'get').returns(blueBird.resolve(JSON.stringify({ "glossary": [{"type": "glossary","lang": "es"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "es"},{"type": "glossary","lang": "es"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "es"},{"type": "glossary","lang": "es"},{"type": "glossary","lang": "en"}]})));
  });

  afterEach(function() {
    request.get.restore();
	});
  
  it('should return an array of english articles when requested', async () => {
    // Add implementation
    const actual = { "glossary": [{"type": "glossary","lang": "en"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "en"}]}; 
    let response = await index.handler("english");
    expect(response).to.deep.equal(actual);
  });

  it('should return an array of spanish articles when requested', async () => {
    // Add implmentation
    const actual = { "glossary": [{"type": "glossary","lang": "es"},{"type": "glossary","lang": "es"},{"type": "glossary","lang": "es"},{"type": "glossary","lang": "es"},{"type": "glossary","lang": "es"}]}; 
    let response = await index.handler("spanish");
    expect(response).to.deep.equal(actual);
  });

  it('should return an array of all articles when no language is specified', async () => {
    // Add implementation
    const actual = { "glossary": [{"type": "glossary","lang": "es"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "es"},{"type": "glossary","lang": "es"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "en"},{"type": "glossary","lang": "es"},{"type": "glossary","lang": "es"},{"type": "glossary","lang": "en"}]}; 
    let response = await index.handler("");
    expect(response).to.deep.equal(actual);
  });
});
