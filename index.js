const request = require('request-promise-native');

const url = 'https://www.healthcare.gov/api/glossary.json';

// return an array of healthcare.gov glossary items for the language specified
// if no language is specified, return an array of all glossary items.
exports.handler = async (language) => {
  const response = await request.get(url);
  const json_response = JSON.parse(response);
  if(language == "") return json_response;
  var lang_glossary = [];
  json_response["glossary"].forEach(object => {
    if(language == "english" && object["lang"] == "en") lang_glossary.push(object);
    if(language == "spanish" && object["lang"] == "es") lang_glossary.push(object);
  });
  json_lang_glossary = {}
  json_lang_glossary["glossary"] = lang_glossary;
  return json_lang_glossary;
};
